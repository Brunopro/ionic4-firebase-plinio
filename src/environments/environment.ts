// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCl8CgsEdBx-jqcFOv44P5YR6Rol0NgxdA",
    authDomain: "ionic4-firebase-plinio.firebaseapp.com",
    databaseURL: "https://ionic4-firebase-plinio.firebaseio.com",
    projectId: "ionic4-firebase-plinio",
    storageBucket: "",
    messagingSenderId: "2844467375",
    appId: "1:2844467375:web:c189664b8cb60bfc05f45a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
